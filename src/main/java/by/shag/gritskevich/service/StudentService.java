package by.shag.gritskevich.service;

import by.shag.gritskevich.api.dto.StudentDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private static int ID_SEQUENCE = 5;

    private List<StudentDto> studentFromDb;

    public StudentService() {
        this.studentFromDb = new ArrayList<>();
        studentFromDb.add(new StudentDto(1, "Vlad", "Gritskevich", 1996));
        studentFromDb.add(new StudentDto(2, "FFF", "RRR", 1997));
        studentFromDb.add(new StudentDto(3, "GGG", "TTT", 1998));
        studentFromDb.add(new StudentDto(4, "YYY", "HHHH", 1994));
    }

    public StudentDto create(StudentDto dto){
        dto.setId(ID_SEQUENCE ++);
        studentFromDb.add(dto);
        return dto;
    }

    public StudentDto findById(Integer id) {
        Optional<StudentDto> desiredStudent =  studentFromDb.stream()
                .filter(st -> st.getId().equals(id))
                .findFirst();
        return desiredStudent.orElseThrow(() -> new RuntimeException("Not found"));
    }

    public List<StudentDto> findAll() {
        return studentFromDb;
    }

    public StudentDto update(StudentDto studentDto) {
        StudentDto persistedStudent = findById(studentDto.getId());
        persistedStudent.setName(studentDto.getName());
        persistedStudent.setLastName(studentDto.getLastName());
        persistedStudent.setYearOfBirth(studentDto.getYearOfBirth());
        return persistedStudent;
    }

    public void deleteById(Integer id) {
        studentFromDb.remove(findById(id));
    }
}
