package by.shag.gritskevich.api;

import org.apache.commons.math3.util.Precision;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigInteger;
import java.util.stream.IntStream;

@Controller
public class MathController {

    @GetMapping("/getSum")
    public String getSum(@RequestParam(name="first", required = false, defaultValue="0") Integer first,
                         @RequestParam(name="second", required = false, defaultValue="0") Integer second,
                         Model model) {
        model.addAttribute("result", first + second);
        return "page.html";
    }

    @GetMapping("/getDiff")
    public String getDiff(@RequestParam(name="first", required = false, defaultValue = "0") Integer first,
                         @RequestParam(name="second", required = false, defaultValue = "0") Integer second,
                         Model model) {
        model.addAttribute("result", first - second);
        return "page.html";
    }

    @GetMapping("/getMultiply")
    public String getMultiply(@RequestParam(name="first", required = false, defaultValue = "0") Integer first,
                          @RequestParam(name="second", required = false, defaultValue = "0") Integer second,
                          Model model) {
        model.addAttribute("result", Precision.round(first * second, 4));
        return "page.html";
    }

    @GetMapping("/getDivision")
    public String getDivision(@RequestParam(name="first", required = false, defaultValue = "0") Integer first,
                              @RequestParam(name="second", required = false) Integer second,
                              Model model) {
        if (second == 0) {
            model.addAttribute("result", "Ошибка, нельзя делить на ноль");
            return "page.html";
        }
        model.addAttribute("result", Precision.round(first / second, 2));
        return "page.html";
    }

    @GetMapping("/getFactorial")
    public String getFactorial(@RequestParam(name="first", required = false, defaultValue = "0") Integer first, Model model) {

        model.addAttribute("result", getFactorial(first));
        return "page.html";
    }

    public static BigInteger getFactorial(int f) {
        if (f < 2) {
            return BigInteger.valueOf(1);
        }
        else {
            return IntStream.rangeClosed(2, f).mapToObj(BigInteger::valueOf).reduce(BigInteger::multiply).get();
        }
    }
}
