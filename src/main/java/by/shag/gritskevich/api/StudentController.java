package by.shag.gritskevich.api;

import by.shag.gritskevich.api.dto.StudentDto;
import by.shag.gritskevich.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/student")
    public String findAll(Model model) {
        List<StudentDto> all = studentService.findAll();
        model.addAttribute("students", all);
        return "students.html";
    }

    @GetMapping(value = "/student/new-student")
    public String getCreateForm(Model model) {
        model.addAttribute("student", new StudentDto());
        return "student-create.html";
    }

    @PostMapping(value = "/student")
    public String createStudent(
            @ModelAttribute("student") StudentDto studentDto, Model model) {
        studentService.create(studentDto);
        return findAll(model);
    }

}
