package by.shag.gritskevich.api;

import by.shag.gritskevich.api.dto.LoginDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String getLoginForm() {
        return "login.html";
    }

    @PostMapping("/login")
    public String login(@RequestParam("login") String login,
                        @RequestParam("password") String password,
                        Model model) {
        if (login.equals("admin") &&
                password.equals("admin")) {
            model.addAttribute("name2", login);
            model.addAttribute("lastname2", login);
            return "greeting.html";
        } else {
            return "login.html";
        }
    }

//    @PostMapping("/login")
//    public String login(@RequestBody LoginDto loginDto, Model model) {
//        if (loginDto.getLogin().equals("admin") &&
//        loginDto.getPassword().equals("admin")) {
//            model.addAttribute("name2", loginDto.getLogin());
//            model.addAttribute("lastname2", loginDto.getLogin());
//            return "greeting.html";
//        } else {
//            return "login.html";
//        }
//    }
}
